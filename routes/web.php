<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tampil','hallo@index');
Route::get('/tambah','hallo@create');
Route::get('/edit/{id}','hallo@edit');
Route::get('/hapus/{id}','hallo@hapus');
Route::post('/update','hallo@update');

Route::resource('/hallo', 'hallo');