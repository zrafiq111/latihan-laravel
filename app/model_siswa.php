<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class model_siswa extends Model
{
    protected $table = 'siswa_latihan';

    protected $fillable =[
    	'nama'
    ];
}
