<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\model_siswa;

class hallo extends Controller
{
    public function index()
    {
        $pegawai = DB::table('siswa_latihan')->get();
        return view('index',['siswa_latihan'=>$pegawai]);
    }
    public function create()
    {
    	return view('form');
    }
    public function edit($id)
    {
        $pegawai = DB::table('siswa_latihan')->where('id',$id)->get();
        return view('edit',['edit' => $pegawai]);
    }
    public function store(Request $request)
    {
    	model_siswa::create($request->all());
        return redirect('/tampil');
    }

    public function update(Request $request)
    {
        DB::table('siswa_latihan')->where('id',$request->id)->update([
            'nama' => $request->nama
        ]);
        return redirect('/tampil');
    }
    public function hapus($id)
    {
        DB::table('siswa_latihan')->where('id',$id)->delete();
        return redirect('/tampil');
    }
}
