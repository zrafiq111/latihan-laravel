@extends('layouts.app')

@section('content')
<form action="/update" method="POST">
	{{ csrf_field() }}
	@foreach($edit as $x)
	<input type="" name="id" value="{{ $x->id }}" hidden="">	
	<input type="" name="nama" value="{{ $x->nama }}">	
	<input type="submit" value="Simpan Data">
	@endforeach
</form>
@endsection