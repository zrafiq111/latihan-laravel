@extends('layouts/app')

@section('content')
<table border="1">
	<tr>
		<th>id</th>
		<th>Nama</th>
		<th>Aksi</th>
	</tr>
	@foreach($siswa_latihan as $x)
	<tr>
		<td>{{ $x->id }}</td>
		<td>{{ $x->nama }}</td>
		<td>
			<a href="/edit/{{ $x->id }}">Edit</a>
			<a href="/hapus/{{ $x->id }}">Hapus</a>
		</td>
	</tr>
	@endforeach
</table>
@endsection